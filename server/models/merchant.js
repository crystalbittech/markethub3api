var mongoose     = require('mongoose');
var Schema       = mongoose.Schema;

var MerchantSchema   = new Schema({
  merchantID: String,
  uid: String,
  bizName: String,
  bizEmail: String,
  photo: String,
  bizStartDate:Number,
  bizPhoneNumber:String,
  bizAdress:String,
  bizCategory:String,
  bizState:String,
  bizLGA:String,
  siteSubdomain:String,
  reglocation:{name:String,lat:Number,lng:Number,center:String},
  settings:{pageHeader:{mainTitle:String,subTitle:String,img:String},pageFooter:[]},
  isActive:false
});

module.exports = mongoose.model('Merchant', MerchantSchema);