var mongoose     = require('mongoose');
var Schema       = mongoose.Schema;

var ChatSchema   = new Schema({
  chatID: String,
  chatTitle:String,
  senderuid: String,
  recieveruid: String,
  datetime:Number,
  lastMsgText: String,
  lastMsgDate: Number,
  recieverName: String,
  chatStatus:Number
});

module.exports = mongoose.model('ChatModel', ChatSchema);