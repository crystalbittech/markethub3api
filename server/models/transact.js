var mongoose     = require('mongoose');
var Schema       = mongoose.Schema;

var TransationSchema   = new Schema({
  tranrequestid: String,
  tranperiod:Number,
  tranmethod:String,
  transactionid: String,
  tranRate:Number,
  tipamount:Number,
  tranAmt: Number,
  tranStatus: String,
  trandescription: String,
  trandate: Number,
  latestTrandate: Number,
  trancredperson:String,
  trandebitperson: String
  ,tranTitle:String
});

module.exports = mongoose.model('Transaction', TransationSchema);