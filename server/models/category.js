var mongoose     = require('mongoose');
var Schema       = mongoose.Schema;

var CategorySchema   = new Schema({
  id: String,
  uid: String,
  categoryName: String,
  classtype:String,
  description: String,
  image: String,
  isAvailable:false
});

module.exports = mongoose.model('Category', CategorySchema);