var mongoose     = require('mongoose');
var Schema       = mongoose.Schema;

var UserSessionSchema   = new Schema({
    uid: String,
    cartDetail:{uid:String,totalPrice:number=0,cartItems:[]},
    presntlocation:{name:String,lat:Number,lng:Number,center:String}
});

module.exports = mongoose.model('UserSession', UserSessionSchema);