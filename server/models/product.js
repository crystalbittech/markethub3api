var mongoose     = require('mongoose');
var Schema       = mongoose.Schema;

var ProdutSchema   = new Schema({
  productID:Number,
  merchantID: String,
  productName: String,
  description: String,
  price: Number,
  image: String,
  sizes:[],
  colors:[],
  isAvailable:false,
  isTopBrand:String,
  prodCategory:String
});

module.exports = mongoose.model('Product', ProdutSchema);