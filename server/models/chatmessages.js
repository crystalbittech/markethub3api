var mongoose     = require('mongoose');
var Schema       = mongoose.Schema;

var ChatMessageSchema   = new Schema({
  msgID: String,
  senderuid: String,
  recieveruid: String,
  msgfrom:String,
  msgto:String,
  msg: String,
  msgtype:String,
  datetime: Number,
  msgstatus:Number
});

module.exports = mongoose.model('ChatMessageModel', ChatMessageSchema);