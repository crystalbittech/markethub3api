var exp 				= require('express');
var app         = exp();
var mongoose    = require('mongoose');
mongoose.Promise = require('bluebird');
var logger      = require('morgan');
var bodyParser  = require('body-parser');
var cors        = require('cors');
const router 	= require('./routes');
var db;
const ur1 = 'mongodb://127.0.0.1:27017/markethub';
const nodemailer = require('nodemailer');
const Email = require('email-templates');

app.use(exp.static('../markethub3api'));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json()); 
app.use(logger('dev')); 
app.use(cors());


mongoose.Promise = global.Promise;
mongoose.connect(ur1,function (err, database){
	if (err) {
		console.log(err);
		console.log("Database connection failed "+err);
		process.exit(1);
	}
	db = database;
	console.log("Database connection to "+ur1+" ready");
	 var server = app.listen(process.env.PORT || 8087, function () {
		var port = server.address().port;
		console.log("App now running on port", port);
	  });
});

router.use(function(req, res, next) {
    // do logging
    console.log('Something about to  Happen.');
    next(); // make sure we go to the next routes and don't stop here
});

app.get('/', (req, res) => {
	res.status(200).json({ message: 'Welcome to PayPorter API' });
});

app.get('/getTime', (req, res) => {
	var currTime = new Date().getTime();
	var dateinwords = new Date().getDate()+" "+new Date().getMonth()+" "+new Date().getUTCFullYear();
	res.status(200).json({ dummy:"result",message: currTime,stringVal:dateinwords });
});

app.get('/api/sendmail', (req, res) => {
	var reqDetails 	= req.body;
	var usrEmail    = 'owolabi.sunday08@gmail.com';//reqDetails.email;
	var mailSubject = 'test';//reqDetails.mailSubject;
	var mailContent = 'gfdsgds';//reqDetails.mailContent;
		var transporter = nodemailer.createTransport({
		service: 'gmail',
		auth: {
		  user: 'digitalfix9@gmail.com',
		  pass: 'wondaful2018'
		}
	  });
		console.log("sending to: "+usrEmail);
		// var template = process.cwd() + '/views/index.jade';
		// var html='';
		// fs.readFile(template, 'utf8', function(err, file){
		// 		if(!err){
		// 			var compiledTmpl = _jade.compile(file, {filename: template});
		// 			var context = {title: 'Express'};
		// 			html = compiledTmpl(context);
		// 		}
		// });
		const email = new Email();
 
		email
			.render('mars/html', {
				name: 'Owolabi'
			})
			.then(renderedHTML=>{
				console.log("Html Message is :"+renderedHTML);
				var mailOptions = {
					from: 'digitalfix9@gmail.com',
					to: usrEmail,
					subject: mailSubject,
					replyTo:'digitalfix9@gmail.com',
					html: renderedHTML
				};
				transporter.sendMail(mailOptions, function(error, info){
					if (error) {
						console.log(error);
						res.json({ message: 'Mail Sending Failed!',error:error});
					} else {
						console.log('Email sent: ' + info.response);
						res.json({ message: 'Mail Sent'});
					}
				 });

			})
			.catch(console.error);

	  
	  
	  
})

app.use('/api', router);
