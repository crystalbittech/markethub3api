var userSessionRoute = require('express').Router();
const UserSession = require ('../models/userSession');
const Merchant = require ('../models/merchant');

userSessionRoute
    .get('/all',function(req, res) {
        UserSession.find(function(err, userSess) {
            if (err)
                res.send(err);
			console.log("Found Session: "+userSess)
            res.json(userSess);
        });
    })
    .get('/:userid',function(req, res) {
        var usrID = req.params.userid;
        console.log("fetching with: "+usrID)
        if(!usrID.includes('HY')){
            UserSession.findOne({uid:usrID}, function(err, userSess) {
                if (err)
                    res.send(err);
                console.log("Found Session: "+userSess)
                var cardDet={uid:usrID,totalPrice:0,cartItems:[]};
                if(userSess!=null&&userSess!=""){
                    cardDet= userSess.cartDetail
                }
                var respp = {
                    isUserLoggedIn:false,
                    uid:usrID,
                    merchantID:'',
                    cartDetails:cardDet
                }
                res.json(respp);
            });
        }else{
            Merchant.findOne({uid:usrID},function(err,merchant){
            if(err){
                res.send(err);
            }else{
                if(merchant!=null&&merchant!=""){
                    UserSession.findOne({uid:usrID}, function(err, userSess) {
                        if (err)
                            res.send(err);
                        console.log("Found User: "+userSess)
                        var cardDet={uid:usrID,totalPrice:0,cartItems:[]};
                        if(userSess!=null&&userSess!=""){
                            cardDet= userSess.cartDetail
                        }
                        var respp = {
                            isUserLoggedIn:true,
                            uid:usrID,
                            merchantID:merchant.merchantID,
                            cartDetails:cardDet
                        }
                        res.json(respp);
                    });
                }else{
                    res.json({});
                }
            }

        })
        }
        
        
    })
    .post('/add',function(req, res) {
        var updData = req.body;
        console.log("Data Recieved is: "+JSON.stringify(updData))
        UserSession.findOne({uid:updData.uid}, function(err, userSess) {
            if (err)
                res.send(err);
            console.log("Found Session Record: "+userSess);
            if(userSess!=null&&userSess!=""){
                userSess.cartDetail     = updData;
                userSess.presntlocation	= updData.presntlocation;
                //Update user settings
                userSess.save(function(err) {
                    if (err)
                        res.send(err);
                    console.log("Usersession updated!");
                    res.json({ message: 'Usersession updated!',data:userSess });
                });

            }else{
                console.log("No Record Found; Saving New Record");
                var mySess            = new UserSession();
                mySess.uid            = updData.uid;
                mySess.cartDetail     = updData;
                mySess.presntlocation = updData.presntlocation;
                //Save user settings
                mySess.save(function(err) {
                    if (err)
                        res.send(err);
                    console.log("Usersession Saved!");
                    res.json({ message: 'Usersession Saved!',data:mySess });
                });
            }
            

        });
    })
	.get('/delete/:userid',function(req, res) {
        UserSession.remove({uid:req.params.userid},function(err,userSess){
            if (err)
                res.send(err);

            res.json({ message: 'Successfully deleted' });
        });
    })
    .get('/delete/delete/all',function(req, res) {
        UserSession.remove(function(err,userSess){
            if (err)
                res.send(err);
            res.json({ message: 'Successfully deleted all' });
        });
    });


    
module.exports = userSessionRoute;
