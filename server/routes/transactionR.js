var transactRoute = require('express').Router();
const Transaction = require ('../models/transact');
const TranHist    = require ('../models/tranhist');
const User = require ('../models/user');

transactRoute
.post('/addtran',function(req, res) {
        var transact = new Transaction();      // create a new instance of the user model
        var tranDetail = {};
        tranDetail = req.body;	
        if(tranDetail.reqid==null||tranDetail.reqid==""||tranDetail.reqid=="undefined"){
            res.json({message:"Null request id sent"});
        }else{
            Transaction.find({tranrequestid:tranDetail.reqid},function(err, tranRecord) {
              if (err)
                  res.json(err);
              if(tranRecord!=""){
                  res.json({ message: 'Transaction Already exists' });
              }else{
                transact.tranrequestid    = tranDetail.reqid;
                transact.trandate         = new Date().getTime();
                transact.latestTrandate   = new Date().getTime();
                transact.tranperiod       = tranDetail.tranperiod;
                transact.tranmethod       = tranDetail.tranmethod;
                transact.tranRate         = tranDetail.tranRate;
                transact.tipamount        = tranDetail.tipamount;
                transact.tranAmt          = tranDetail.tranAmt;
                transact.trandescription  = tranDetail.trandescription;
                transact.trancredperson   = tranDetail.trancredpersonID;
                transact.trandebitperson  = tranDetail.trandebitpersonID;
                transact.tranStatus       = tranDetail.tranStatus;

                transact.save(function(err) {
                      if (err)
                          res.send(err);
                    var tranhist = new TranHist();
                    tranhist.tranrequestid    = tranDetail.reqid;
                    tranhist.latestTrandate   = transact.latestTrandate;
                    tranhist.tranStatus       = 0;
                    tranhist.statusDescriptn  = "INITIATED";
                    tranhist.save(function(err) {
                        if (err)
                            res.send(err);
                        res.json({ message: 'Transaction Initiated ID:'+tranhist.tranrequestid,data:transact});
                    });
                  });
              }
          });
        }
        
})
.post('/addstatus',function(req, res) {
    var tranDetail = {};
    tranDetail = req.body;	
    if(tranDetail.tranid==null||tranDetail.tranid==""||tranDetail.tranid=="undefined"){
        res.json({message:"Invalid Request ID Sent"});
    }else{
        TranHist.find({tranrequestid:tranDetail.tranid},function(err, tranRecord) {
          if (err)
              res.json(err);
          if(tranRecord==""){
              res.json({ message: 'Transaction ID is Invalid' });
          }else{
            var alreadyExist=false;
            for(let i = 0; i < tranRecord.length; i++){
                if(tranRecord[i].tranStatus==tranDetail.tranStatus){
                    alreadyExist = true;
                }
            }
            if(alreadyExist){
                res.json({ message: 'Status Already Exist'});
            }else{
                var tranhist = new TranHist();
                tranhist.tranrequestid    = tranDetail.tranid;
                tranhist.latestTrandate   = new Date().getTime();
                tranhist.tranStatus       = tranDetail.tranStatus;
                tranhist.statusDescriptn  = tranDetail.description;
                tranhist.save(function(err) {
                    if (err)
                        res.send(err);
                    //Update transaction last status
                    Transaction.findOne({tranrequestid:tranDetail.tranid},function(err,tran){
                        if(err)
                            res.send(err);
                        tran.tranStatus = tranDetail.description;
                        tran.save(function(err){
                            if (err)
                                res.send(err);
                            res.json({ message: 'Tran Status Added!',data:tranhist});
                        })
                    })
                    
                });
            }
          }
        });
    }
    
})
.get('/fetchtran',function(req, res) {
    console.log("Querying Transaction Table using senderID: "+req.query.senderID);
    var searhKey;
    var tranList=[];
    var senderID    = req.query.senderID;
    var lstMsgDate  = req.query.msgDate;
    if(lstMsgDate==null||lstMsgDate==""){
        lstMsgDate = 0;
    }
    // Transaction.find({$or:[{trancredperson:senderID},{trandebitperson:senderID}],latestTrandate:{ $gte: lstMsgDate }})
    Transaction.find({$or:[{trancredperson:senderID},{trandebitperson:senderID}],latestTrandate:{ $gte: lstMsgDate }})
    .then(function(tranDetails){
            var tranItems=[];
            tranDetails.forEach(function(u) {
              // console.log(JSON.stringify(u))
              if(u.trancredperson==senderID){
                searhKey = u.trandebitperson
              }else{
                searhKey = u.trancredperson
              }
              tranItems.push(
                User.findById(searhKey, function(err, user) {
                  if (err){
                    res.send(err);
                  }	
                  u.tranTitle = user.username;
                  tranList.push(u);
                })
              );
            });
            return Promise.all(tranItems);
        })
        .then(resp=>{
            res.json(tranList);
        }).catch(err=>{
            var fatErr = "Reason for failure is: "+err;
            res.json({ message: fatErr });
        });
})
.get('/fetchhist/:tranreqID',function(req, res) {
    var tranID = req.params.tranreqID;
    TranHist.find({tranrequestid:tranID},function(err, statList) {
        if (err)
            res.send(err);
        res.json(statList);
    });
})


.get('/all',function(req, res) {
  Transaction.find(function(err, tranList) {
      if (err)
          res.send(err);

      res.json(tranList);
  });
})
.get('/delete',function(req, res) {
  Transaction.remove(function(err, tranList) {
      if (err)
          res.send(err);
      res.json(tranList);
  });
})
.put('/',function(req, res) {
    Transaction.findOne({tranrequestid:req.params.tranID}, function(err, trans) {

        if (err)
            res.send(err);

        trans.tranStatus = req.body.tranStatus;  // update the bears info
        trans.latestTrandate = new Date().getTime();
        // save the transaction
        trans.save(function(err) {
            if (err)
                res.send(err);

            res.json({ message: 'Transaction updated!',data: trans});
        });

    });
})

module.exports = transactRoute;
