var indexRouter         = require('express').Router();
const usersRoute        = require('../routes/usersR');
const userSessionRoute     = require('../routes/userSessionR');
const chatRoute         = require('../routes/chatR');
const chatmessageRoute  = require('../routes/chatmessageR');
const transactRoute     = require('../routes/transactionR');
const mapRoute          = require('../routes/mapR');
const productRoute      = require('../routes/prodcutR');
const categoryRoute     = require('../routes/categoryR');
const merchantRoute     = require('../routes/merchantR');
const searchRoute     = require('../routes/search');
const mailRoute     = require('../routes/mail');

/* GET home page. */
indexRouter.get('/', (req, res) => {
  res.status(200).json({ message: 'Welcome to My restful service' });
});

indexRouter.use('/user',usersRoute);
indexRouter.use('/session',userSessionRoute);
indexRouter.use('/chat',chatRoute);
indexRouter.use('/chatmessage',chatmessageRoute);
indexRouter.use('/transact',transactRoute);
indexRouter.use('/map',mapRoute);
indexRouter.use('/product',productRoute);
indexRouter.use('/category',categoryRoute);
indexRouter.use('/merchant',merchantRoute);
indexRouter.use('/search',searchRoute);
indexRouter.use('/sendmail',mailRoute);
module.exports = indexRouter;
