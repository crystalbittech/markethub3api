var chatRoute = require('express').Router();
const ChatModel = require ('../models/chat');
const User = require ('../models/user');


chatRoute
.post('/add',function(req, res) {
	var chatmodel;
	var chatDetail = {};
	chatDetail = req.body;
	var chatid = chatDetail.senderuid + chatDetail.recieveruid;
	console.log("Querying Table with "+chatid);
	// ChatModel.find({senderuid:chatDetail.senderuid,recieveruid:chatDetail.recieveruid}, function(err, chatDetails) {
		ChatModel.findOne({chatID:chatid}, function(err, chatDetails) {
            if (err)
                res.send(err);
			if(chatDetails!=null){
				//Updates
				chatDetails.lastMsgText = chatDetail.lastMsgText;
				chatDetails.lastMsgDate = new Date().getTime();
				chatDetails.chatStatus = 0;//not seen
				chatDetails.save(function(err) {
					if (err)
						res.send(err);
					console.log("Chat Updated: ");
					res.json({ message: 'chat updated!' });
				});
			}else{
				//Creates
				chatmodel = new ChatModel();
				chatmodel.chatID 		= chatid;
				chatmodel.chatTitle 	= chatDetail.chatTitle;
				chatmodel.senderuid    	= chatDetail.senderuid;
				chatmodel.recieveruid   = chatDetail.recieveruid;
				chatmodel.datetime  	= new Date().getTime();
				chatmodel.lastMsgText 	= chatDetail.lastMsgText;
				chatmodel.lastMsgDate 	= new Date().getTime();
				chatmodel.recieverName  = chatDetail.recieverName;
				chatmodel.chatStatus	= 0;

				chatmodel.save(function(err) {
					if (err)
						res.send(err);
					res.json({ message: 'Chat Inserted!' });
				});
			}
            
	});
})
.get('/fetch',function(req, res) {
	console.log("Querying Chat Table using senderID: "+req.query.senderID);
	var searhKey;
	var chatList=[];
	// var chatid 		= req.query.chatid;
	var senderID 	= req.query.senderID;
	var lstMsgDate 	= req.query.msgDate;
	if(lstMsgDate==null||lstMsgDate==""){
        lstMsgDate = 0;
	}
	// ChatModel.find({chatID:chatid})
	// ChatModel.find({chatID:chatid,lastMsgDate:{$gte:lstMsgDate}})
		// ChatModel.find({$and:[{chatID:chatid},{lastMsgDate:{$gte:lstMsgDate}}]})
		ChatModel.find({$or:[{senderuid:senderID},{recieveruid:senderID}],lastMsgDate:{ $gte: lstMsgDate }}
		)
		// ChatModel.find({$or:[{senderuid:senderID},{recieveruid:senderID}]}
		// )
		.then(function(chatDetails) {
		var chatItems = [];
		chatDetails.forEach(function(u) {
			if(u.senderuid==senderID){
				searhKey = u.recieveruid
			}else{
				searhKey = u.senderuid
			}
			chatItems.push(
				User.findById(searhKey, function(err, user) {
				if (err){
					res.send(err);
				}
				u.recieverName = user.username;
				chatList.push(u);
			}));
		});
		return Promise.all(chatItems);
	})
	.then(resp=> {
		console.log(JSON.stringify(chatList))
		res.json(chatList);
	}).catch(err=> {
		var fatErr="Reason for failure is: "+err;
		console.log(fatErr);
		res.json({ message: fatErr });
	});
})


.get('/all',function(req, res) {
	ChatModel.find(function(err, chatList) {
		if (err)
			res.send(err);
  
		res.json(chatList);
	});
  })
.get('/delete',function(req, res) {
	ChatModel.remove(function(err, chatList) {
		if (err)
			res.send(err);
		res.json(chatList);
	});
});
//This would be used for chat settings like changing title,image e.t.c
// .put('/',function(req, res) {
// 		var chatDetail = {};
// 		chatDetail = req.body;
//         ChatModel.find({senderuid:chatDetail.senderuid,recieveruid:chatDetail.recieveruid}, function(err, chatmodel) {
//             if (err)
//                 res.send(err);
//             chatmodel.lastMsgText = chatDetail.lastMsgText;
// 			      chatmodel.lastMsgDate = chatDetail.lastMsgDate;
//             chatmodel.save(function(err) {
//                 if (err)
//                     res.send(err);
// 				        console.log("Chat Updated: ");
//                 res.json({ message: 'chat updated!' });
//             });

//         });
//  })
    
module.exports = chatRoute;
