var userRoute = require('express').Router();
const User = require ('../models/user');
var bcrypt = require('bcrypt');
const mySalt  = "$2b$10$IR3o5/Dst8fUSjazprcKCe";
userRoute
.get('/',function(req, res) {
    User.find(function(err, users) {
        if (err)
            res.send(err);
        res.json(users);
    });
})
.get('/storeusers/:merchantID',function(req, res) {
    var searchKey = req.params.merchantID
    console.log("Search using: "+searchKey);
    User.find({merchantID:searchKey},function(err, users) {
        if (err)
            res.send(err);
        res.json(users);
    });
})
.post('/signin',function(req, res) {
    var signInDet = {};
    signInDet = req.body;
    console.log("Signing in with "+signInDet.email+" and "+signInDet.password);
    User.findOne({email:signInDet.email},function(err, user) {
        if (err)
            res.json(err);
        if(user!=""&&user!=null){
            console.log("Hashed password is: "+user.passkey);
            if(bcrypt.compareSync(signInDet.password, user.passkey)){
                res.json({ message: 'user exists',data:user});
            }else{
                res.json({ message: 'Invalid Credentials'});
            }  
        }else{
            res.json({ message: 'Invalid User' });
        }
    });

})
.post('/signup',function(req,res){
    var user = new User();      // create a new instance of the user model
    var signUpDet = {};
    signUpDet       = req.body;	
    User.count({},function(err,usrCount){
        return usrCount;
    }).then(resp=>{
        var idGenerated = "HY"+new Date().getDate()+""+new Date().getMonth()+""+new Date().getUTCFullYear()+resp;
        console.log("Searching with "+signUpDet.email+" and"+idGenerated);
        User.find({$or:[{email:signUpDet.email},{uid:idGenerated}]},function(err, users) {
            if (err)
                res.json(err);
            if(users!=""){
                res.json({ message: 'Already exists' });
            }else{
                user.uid        = idGenerated;
                user.username   = signUpDet.firstname;
                user.phoneNumber= signUpDet.phoneNumber;
                user.firstname	= signUpDet.firstname;
                user.lastname 	= signUpDet.lastname;
                user.birthdate	= signUpDet.birthdate;
                user.address	= signUpDet.address;
                user.usrType    = signUpDet.usrType;
                user.passkey  	= bcrypt.hashSync(signUpDet.password, mySalt);
                user.email	 	= signUpDet.email;
                user.photo		= signUpDet.photo;
                user.isActive   = false;
                user.merchantID = "false";
                
                user.save(function(err) {
                    if (err)
                        res.send(err);
                    res.json({ message: 'user created!',data:user});
                });
            }
        });
    })
    
})
.get('/:userid',function(req, res) {
    console.log("fetching with: "+req.params.userid)
    User.find({uid:req.params.userid}, function(err, user) {
        if (err)
            res.send(err);
    console.log("Found User: "+user)
        res.json(user);
    });
})
.put('/:userid',function(req, res) {
    var updData = req.body;
    //work on updating based on keys in json sent: to reduce traffic content
    // use our user model to find the user we want
    User.findOne({uid:req.params.userid}, function(err, user) {
        if (err)
            res.send(err);
    if(updData.username!=null&&updData.username!=""){
        user.username 		= updData.username;
    }
    user.phoneNumber  	= updData.phonenumber;
    user.passkey  		= updData.password;
    user.firstname		= updData.firstname;
    user.lastname 		= updData.lastname;
    user.birthdate	 	= updData.birthdate;
    user.address	 	= updData.address;
    user.email	 		= updData.email;
    user.isAgent		= updData.isAgent;
    user.photo			= updData.photo;
    
        // update the user
        user.save(function(err) {
            if (err)
                res.send(err);

            res.json({ message: 'user updated!' });
        });

    });
})
.put('/delete/:userid',function(req, res) {
    User.findOne({uid:req.params.userid}, function(err, user) {
        if (err)
            res.send(err);      
        // update the user
        user.isActive = false;
        user.save(function(err) {
            if (err)
                res.send(err);
            res.json({ message: 'user deleetd!' });
        });

    });
})
.get('/delete/all',function(req, res) {
    User.remove(function(err, user) {
            if (err)
                res.send(err);
        console.log("Deleted User: "+user)
        res.json(user);
    });
})
    
    
module.exports = userRoute;
