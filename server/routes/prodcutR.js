var productRoute = require('express').Router();
const Product = require ('../models/product');

productRoute
.get('/all/:filterKey',function(req, res) {
    Product.find(function(err, Products) {
        if (err)
            res.send(err);

        res.json(Products);
    });
})
.get('/storeproduct/:storeId',function(req, res) {
    console.log("Filter Product usng filter "+req.params.storeId)
    if(req.params.storeId==null||req.params.storeId==""){
        res.json({message:"Invalid productID"});
    }else if(req.params.storeId=="mylocation"){
        Product.find(function(err, Products) {
            if (err)
                res.send(err);
            res.json(Products);
        });
    }else{
        Product.find({merchantID:req.params.v},function(err, Products) {
            if (err)
                res.send(err);
            res.json(Products);
        });
    }     
})
.post('/add',function(req,res){
    var productM = new Product()      // create a new instance of the Product model
    var productDetail = {};
    productDetail = req.body;	
    var storeID = productDetail.uid;
    var prodID = productDetail.productID;
    console.log("Searching with "+storeID+" and "+prodID);
    Product.findOne({$and:[{merchantID:storeID},{productID:prodID}]},function(err, Products) {
        if (err)
            res.json(err);
        console.log(Products);
        if(Products!=null&&Products!="null"){
            console.log('Already exists');
            Products.productName    = productDetail.productName;
            Products.price          = productDetail.price;
            Products.image  	    = productDetail.image;
            Products.description  	= productDetail.description;
            Products.isTopBrand     = productDetail.isTopBrand;
            Products.prodCategory  	= productDetail.prodCategory;
            Products.sizes	        = productDetail.sizes;
            Products.colors 	    = productDetail.colors;
            Products.isAvailable    = true;
            
            Products.save(function(err) {
                if (err)
                    res.send(err);
                res.json({ message: 'Product Updated!',data:Products});
            });
        }else{
            Product.count({},function(err,prodCount){
                return prodCount;
            }).then(resp=>{
                productM.merchantID         = storeID;
                productM.productID   = new Date().getDate()+""+new Date().getMonth()+""+new Date().getUTCFullYear()+resp;
                productM.productName = productDetail.productName;
                productM.price       = productDetail.price;
                productM.image  	 = productDetail.image;
                productM.description = productDetail.description;
                productM.isTopBrand  = productDetail.isTopBrand;
                productM.prodCategory= productDetail.prodCategory;
                productM.sizes	     = productDetail.sizes;
                productM.colors 	 = productDetail.colors;
                productM.isAvailable = true;

                productM.save(function(err) {
                    if (err)
                        res.send(err);
                    res.json({ message: 'Product created!',data:productM});
                });
            });
            
        }
    });
})
.get('/:Productid',function(req, res) {
    console.log("fetching with: "+req.params.Productid)
    Product.find({productID:req.params.Productid}, function(err, Product) {
        if (err){
            res.send(err);
        }else{
            console.log("Found Product: "+Product)
            res.json(Product);
        }
    });
})
.put('/:Productid',function(req, res) {
    var updData = req.body;
    //work on updating based on keys in json sent: to reduce traffic content
    // use our Product model to find the Product we want
    Product.findOne({productID:req.params.Productid}, function(err, Product) {
        if (err)
            res.send(err);
    if(updData.Productname!=null&&updData.Productname!=""){
        Product.Productname 		= updData.Productname;
    }
    Product.phoneNumber  	= updData.phonenumber;
    Product.passkey  		= updData.password;
    Product.firstname		= updData.firstname;
    Product.lastname 		= updData.lastname;
    Product.birthdate	 	= updData.birthdate;
    Product.address	 	= updData.address;
    Product.email	 		= updData.email;
    Product.isAgent		= updData.isAgent;
    Product.photo			= updData.photo;
    
        // update the Product
        Product.save(function(err) {
            if (err)
                res.send(err);

            res.json({ message: 'Product updated!' });
        });

    });
})
.put('/delete/:Productid',function(req, res) {
    Product.findOne({productID:req.params.Productid}, function(err, Product) {
        if (err)
            res.send(err);      
        // update the Product
        Product.isActive = false;
        Product.save(function(err) {
            if (err)
                res.send(err);
            res.json({ message: 'Product deleetd!' });
        });

    });
})
.get('/delete/all',function(req, res) {
    Product.remove(function(err, Product) {
            if (err)
                res.send(err);
        console.log("Deleted Product: "+Product)
        res.json(Product);
    });
})
    
    
module.exports = productRoute;
