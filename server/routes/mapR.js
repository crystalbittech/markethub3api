var mapRoute = require('express').Router();
const mapModel = require ('../models/map');

/* GET users listing. */
mapRoute
.post('/',function(req, res) {
  var lng = req.body.lng;
  var lat = req.body.lat;
  var maxDistance = req.body.maxDistance * 1000; //kn

  MapModel.find({

      loc: {

          $near: {
              $geometry: {
                  type: "Point",
                  coordinates: [lng, lat]
              },
              $maxDistance: maxDistance
          },

      }

  }, function(err, markers){

      if(err){
    console.log(JSON.stringify(err))
          res.send(err);
      } else {
          res.json(markers);
    console.log(JSON.stringify(markers))
      }

  });
});
module.exports = mapRoute;
