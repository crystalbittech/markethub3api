var categoryRoute = require('express').Router();
const prodCategory = require ('../models/category');

categoryRoute
.get('/',function(req, res) {
    prodCategory.find(function(err, categories) {
        if (err)
            res.send(err);

        res.json(categories);
    });
})
.get('/filter/:storeID',function(req, res) {
    prodCategory.find({uid:req.params.storeID},function(err, categories) {
        if (err)
            res.send(err);

        res.json(categories);
    });
})
.post('/add',function(req,res){
    var categoryM = new prodCategory()      // create a new instance of the prodCategory model
    var categoryDetail = {};
    categoryDetail = req.body;	
    console.log("Input Recieved is: "+JSON.stringify(categoryDetail))
    var catID;
    if(categoryDetail.id==null||categoryDetail.id==""){
        catID = "ct"+new Date().getTime();
    }else{
        catID = categoryDetail.id
    }
    console.log("Searching with "+categoryDetail.uid+" and "+catID);
    prodCategory.findOne({uid:categoryDetail.uid,id:catID},function(err, categories) {
        if (err){
            res.json(err);
        }
        console.log(categories);
        if(categories!=null&&categories!="null"){
            console.log('Category  exists');
            categories.categoryName    = categoryDetail.categoryName;
            categories.image  	    = categoryDetail.image;
            categories.classtype  	    = categoryDetail.classtype;            
            categories.description  	= categoryDetail.description;
            categories.isAvailable    = true;
            
            categories.save(function(err) {
                if (err)
                    res.send(err);
                res.json({ message: 'prodCategory Updated!',data:categories});
            });
        }else{
            console.log('New Category  Record');
            categoryM.uid         = categoryDetail.uid;
            categoryM.id          = catID;
            categoryM.categoryName= categoryDetail.categoryName;
            categoryM.image  	  = categoryDetail.image;
            categoryM.classtype   = categoryDetail.classtype;  
            categoryM.description = categoryDetail.description;
            categoryM.isAvailable = true;

            categoryM.save(function(err,savedCat) {
                if (err)
                    res.send(err);
                else{
                    res.json({ message: 'prodCategory created!',data:savedCat});
                }
               
            });
        }
    });
})
.get('/find/:CategoryId',function(req, res) {
    console.log("fetching with: "+req.params.Productid)
    prodCategory.find({uid:req.params.Productid}, function(err, prodCategory) {
        if (err)
            res.send(err);
    console.log("Found prodCategory: "+prodCategory)
        res.json(prodCategory);
    });
})
.put('/:CategoryId',function(req, res) {
    var updData = req.body;
    //work on updating based on keys in json sent: to reduce traffic content
    // use our prodCategory model to find the prodCategory we want
    prodCategory.findOne({uid:req.params.Productid}, function(err, prodCategory) {
        if (err)
            res.send(err);
    if(updData.Productname!=null&&updData.Productname!=""){
        prodCategory.Productname 		= updData.Productname;
    }
    prodCategory.phoneNumber  	= updData.phonenumber;
    prodCategory.passkey  		= updData.password;
    prodCategory.firstname		= updData.firstname;
    prodCategory.lastname 		= updData.lastname;
    prodCategory.birthdate	 	= updData.birthdate;
    prodCategory.address	 	= updData.address;
    prodCategory.email	 		= updData.email;
    prodCategory.isAgent		= updData.isAgent;
    prodCategory.classtype  	    = updData.classtype;  
    prodCategory.photo			= updData.photo;
    
        // update the prodCategory
        prodCategory.save(function(err) {
            if (err)
                res.send(err);

            res.json({ message: 'prodCategory updated!' });
        });

    });
})
.put('/delete/:CategoryId',function(req, res) {
    prodCategory.findOne({uid:req.params.Productid}, function(err, prodCategory) {
        if (err)
            res.send(err);      
        // update the prodCategory
        prodCategory.isActive = false;
        prodCategory.save(function(err) {
            if (err)
                res.send(err);
            res.json({ message: 'prodCategory deleetd!' });
        });

    });
})
.get('/delete/all',function(req, res) {
    prodCategory.remove(function(err, prodCategory) {
            if (err)
                res.send(err);
        console.log("Deleted prodCategory: "+prodCategory)
        res.json(prodCategory);
    });
})
    
    
module.exports = categoryRoute;
