var merchantRoute = require('express').Router();
const Merchant = require ('../models/merchant');
const User = require ('../models/user');
const Product = require ('../models/product');
const Category = require ('../models/category');

merchantRoute
.get('/all',function(req, res) {
    Merchant.find(function(err, merchants) {
        if (err)
            res.send(err);

        res.json(merchants);
    });
})
.get('/admin/:usrID',function(req, res) {
    console.log("fetching with: "+req.params.usrID)
    Merchant.findOne({uid:req.params.usrID}, function(err, merchant) {
        if (err){
             res.send(err);
        }else if(merchant!=""&&merchant!=null){
            // console.log("Found Merchant: "+merchant)
            res.json({message: 'found', data: merchant.merchantID});
        }else{
            res.json({message: 'Not found'});
        }
    });
})
.get('/storeinfo/:storeID',function(req, res) {
    console.log("Filter Store usng filter "+req.params.storeID)
    if(req.params.storeID==null||req.params.storeID==""){
        res.json({message:"Invalid StoreID"});
    }else{
        Merchant.findOne({merchantID:req.params.storeID},function(err, merchant) {
            if (err)
                res.send(err);
            Product.find({merchantID:req.params.storeID},function(err,products){
                if(err)
                    res.json(err)
                User.find({merchantID:req.params.storeID},function(er,users){
                    if(err)
                        res.json(err)
                    Category.find({uid:req.params.storeID},function(err,categories){
                        if(err)
                            res.json(err)
                        res.json({respcode:"00",message:"Inquired Succcessfully",data:{store:merchant,products:products,users:users,categories:categories}});
                    })
                })   
            });
        });
    }    
})
.post('/add',function(req,res){
    var merchant = new Merchant();
    var merchantDet = {};
    var pageHeader = {mainTitle:"",subTitle:"",img:''}
    var pageFooter = [
          {isActivated:true,columnTitle:"Contact Us",columnType:'',columnDescriptn:"Any questions? Let us know in store at 8th floor, 379 Hudson St, New York, NY 10018"},
          {isActivated:true,columnTitle:"Categories",columnType:'',columnDescriptn:""},
          {isActivated:true,columnTitle:"Links",columnType:'',columnDescriptn:""},
          {isActivated:false,columnTitle:"Help",columnType:'',columnDescriptn:""},
          {isActivated:true,columnTitle:"Newsletter",columnType:'',columnDescriptn:""}];
    merchantDet     = req.body;	
    var idGenerated = merchantDet.uid;
    console.log("Searching with "+merchantDet.email+" and "+idGenerated);
    Merchant.findOne({bizEmail:merchantDet.email},function(err, merchants) {
        if (err)
            res.json(err);
        if(merchants!="" && merchants!=null){
            res.json({ message: 'Merchant Already exists' });
        }else{
            Merchant.count({},function(err,res){
                return res;
            }).then(resp=>{
                merchant.merchantID     = "ST"+new Date().getDate()+""+new Date().getMonth()+""+new Date().getUTCFullYear()+resp;;
                merchant.uid            = merchantDet.uid;
                merchant.bizName        = merchantDet.businessName;
                merchant.bizPhoneNumber = merchantDet.bizPhoneNumber;
                merchant.siteSubdomain 	= merchantDet.siteSubdomain;
                merchant.bizAdress	    = merchantDet.bizAdress;
                merchant.bizCategory 	= merchantDet.bizCategory;
                merchant.bizState 	    = merchantDet.bizState;
                merchant.bizLGA 	    = merchantDet.bizLGA;
                merchant.bizStartDate	= merchantDet.bizStartDate;
                merchant.bizEmail	 	= merchantDet.email;
                merchant.photo		    = merchantDet.photo;
                merchant.settings       = {pageHeader:pageHeader,pageFooter:pageFooter};
                merchant.isActive       = true;
                
                merchant.save(function(err) {
                    if (err){
                        res.send(err);
                    }
                    else{
                        User.findOneAndUpdate({uid:merchant.uid},{merchantID:merchant.merchantID},function(err){
                            if(err){
                                res.send(err);
                            }else{
                                res.json({ message: 'merchant created!',data:merchant});
                            }
                        }) 
                    }   
                });
            })
            
        }
    });
})
.put('/update/:merchantID',function(req, res) {
    var updData = req.body;
    Merchant.findOne({merchantID:req.params.merchantID}, function(err, merchant) {
        if (err)
            res.send(err);
        if(merchant==""){
            res.json({ message: 'Merchant Does Not Exists' });
        }else{
            if(updData.businessName!=null&&updData.businessName!=""){
                merchant.bizName 		= updData.businessName;
            }
            merchant.bizEmail  	    = updData.email;
            merchant.bizLGA  		= updData.bizLGA;
            merchant.bizState		= updData.bizState;
            merchant.bizCategory 	= updData.bizCategory;
            merchant.bizAdress	 	= updData.bizAdress;
            merchant.bizPhoneNumber	= updData.bizPhoneNumber;
            merchant.bizStartDate	= updData.bizStartDate;
            merchant.siteSubdomain	= updData.siteSubdomain;
            merchant.bizStartDate	= updData.bizStartDate;
            merchant.isActive		= updData.isActive;
            merchant.photo			= updData.photo;
            merchant.save(function(err) {
                if (err)
                    res.send(err);
                res.json({ message: 'Merchant Updated!' });
            });
        }
    });
})
.put('/updateprop',function(req, res) {
    var updData = req.body;
    Merchant.findOne({merchantID:updData.merchantID}, function(err, merchant) {
        if (err)
            res.send(err);
        if(merchant==""){
            res.json({ message: 'Merchant Does Not Exists' });
        }else{
            // console.log("Now updating with: "+updData.merchantProp)
            Merchant.update({merchantID:updData.merchantID},
                {settings:updData.merchantProp},function(err) {
                if (err)
                    res.send(err);
                res.json({ message: 'Merchant Properties Updated!' });
            });
        }
    });
})
.put('/delete/:merchantID',function(req, res) {
    Merchant.findOne({uid:req.params.merchantID}, function(err, merchant) {
        if (err)
            res.send(err);      
        // update the merchant
        merchant.isActive = false;
        merchant.save(function(err) {
            if (err)
                res.send(err);
            res.json({ message: 'merchant deleetd!' });
        });

    });
})
.get('/delete/all',function(req, res) {
    Merchant.remove(function(err, merchant) {
            if (err)
                res.send(err);
        console.log("Deleted Merchant: "+merchant)
        res.json(merchant);
    });
})
    
    
module.exports = merchantRoute;
