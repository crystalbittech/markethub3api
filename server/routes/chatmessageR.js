var chatmessageRoute = require('express').Router();
const ChatMessageModel = require ('../models/chatmessages');

chatmessageRoute
.post('/add',function(req, res) {
  var chatDetail = {};
  chatDetail = req.body;
  var chatMessage = new ChatMessageModel();
  var msgid       = chatDetail.senderuid+chatDetail.recieveruid;
  console.log("ID is: "+msgid);
  chatMessage.msgID         =  msgid;	
  chatMessage.senderuid     = chatDetail.senderuid;
  chatMessage.recieveruid   = chatDetail.recieveruid;
  chatMessage.msgfrom       = chatDetail.from;
  chatMessage.msgto         = chatDetail.to;
  chatMessage.msg           = chatDetail.body;
  chatMessage.msgtype       = chatDetail.type;
  chatMessage.datetime      = new Date().getTime();
  chatMessage.msgstatus     = chatDetail.status;
  chatMessage.save(function(err) {
    if (err){
        res.send(err);
    }else{
        console.log("Message Inserted");
        res.json({ message: 'Message Inserted!' });
    }
      
  });		
})
.get('/fetch',function(req, res) {
    var senderID = req.query.senderuid;
    var recvID = req.query.recieveruid;
    var lstMsgDate = req.query.msgDate;
    console.log("Fetching Using: senderID "+senderID+" and recieverID "+recvID+" and msgDate "+lstMsgDate);
    if(lstMsgDate==null||lstMsgDate==""){
        lstMsgDate = 0;
    }
    // ChatMessageModel.find({},
    //     function(err, chatmessage) {
    ChatMessageModel.find({senderuid:{$in:[senderID,recvID]}
                        ,recieveruid:{$in:[senderID,recvID]}
                        ,datetime:{$gte:lstMsgDate}}, function(err, chatmessage) {
        if (err)
            res.send(err);
        res.json(chatmessage);
    });
})
.put('/update/:msgid',function(req, res) {
    ChatMessageModel.findOne(req.params.msgid, function(err, chatmessage) {
        if (err)
            res.send(err);
        chatmessage.msgstatus = 1;  // not read//should be passed from input as read,not sent e.t.c
        chatmessage.save(function(err) {
            if (err)
                res.send(err);
            res.json({ message: 'chat updated!' });
        });
    });
})
// .get('/:searchkey',function(req, res) {
//   var searchkey = req.params.searchkey;
//       ChatMessageModel.find({$or:[{senderuid:searchkey},{recieveruid:searchkey}]}, function(err, chatmessage) {
//           if (err)
//               res.send(err);
//           res.json(chatmessage);
//       });
// })
.get('/all',function(req, res) {
	ChatMessageModel.find(function(err, chatMsgList) {
		if (err)
			res.send(err);
		res.json(chatMsgList);
	});
})
.get('/delete',function(req, res) {
    ChatMessageModel.remove(function(err, chatMsgList) {
        if (err)
            res.send(err);
        res.json(chatMsgList);
    });
});

module.exports = chatmessageRoute;
