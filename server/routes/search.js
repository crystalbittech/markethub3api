var searchRoute = require('express').Router();
const Product = require ('../models/product');
const Merchant = require ('../models/merchant');
searchRoute
.get('/:searchQuery',function(req, res) {
    var searchQuery = req.params.searchQuery;
    console.log("Recieved: "+searchQuery);
    searchQuery=searchQuery.replace(/ +(?= )/g,'').trim();
    console.log("Search query now:"+searchQuery);
    var searchResult=[];
	// var searchItems = searchQuery.split(" ");
	// for(var i=0;i<searchItems.length;i++){
    //     console.log("Spaced content is: "+searchItems[i]);
    //     var prodName = searchItems[i].trim();
		Product.find({productName:{'$regex': '.*'+searchQuery+'.*'}},function(err,products){
            console.log("Found: "+products);
            if(products!=""&&products!=null){
                
                searchResult.push({searcType:'product',searchID:products.productID,metadata:products});
            }
            Merchant.find({bizName:{'$regex': '.*'+searchQuery+'.*'}},function(err,merchants){
                console.log("Found: "+merchants);
                if(merchants!=""&&merchants!=null){
                    searchResult.push({searcType:'store',searchID:merchants.merchantID,metadata:merchants});
                }
                console.log("Exiting:");
                 var itmFound = false;
                 if(searchResult.length>0){
                    itmFound = true;
                 }
                 res.status(200).json({ found:itmFound,message: searchResult});
            }).catch(err=>{
                res.status(200).json({ found:false,message: err});
            })
        }).catch(err=>{
            res.status(200).json({ found:false,message: err});
        })
    // }
    
})
module.exports = searchRoute;
