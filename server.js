var exp 		= require('express');
var app         = exp();
var mongoose    = require('mongoose');
mongoose.Promise = require('bluebird');
var logger      = require('morgan');
var bodyParser  = require('body-parser');
var cors        = require('cors');
const router 	= require('./server/routes');
var session 	= require('express-session');
var db;
// const ur1 = 'mongodb://127.0.0.1:27017/markethub';
const ur1 = 'mongodb://admin1:admin123@ds245680.mlab.com:45680/markethub';


// app.use(bodyParser.urlencoded({ extended: false }));
// app.use(bodyParser.json()); 

app.use(bodyParser.json({limit: '50mb'}));
app.use(bodyParser.urlencoded({limit: '50mb', extended: true}));
app.use(logger('dev')); 
app.use(cors());
app.use(session({secret: "Shh, its a secret!"}));

mongoose.Promise = global.Promise;
mongoose.connect(ur1,function (err, database){
	if (err) {
		console.log(err);
		console.log("Database connection failed "+err);
		process.exit(1);
	}
	db = database;
	console.log("Database connection to "+ur1+" ready");
	 var server = app.listen(process.env.PORT || 8087, function () {
		var port = server.address().port;
		console.log("App now running on port", port);
	  });
});

router.use(function(req, res, next) {
    // do logging
    console.log('Something about to  Happen.');
    next(); // make sure we go to the next routes and don't stop here
});

app.get('/', (req, res) => {

	if(req.session.page_views){
		var numOfTimeVisited = req.session.page_views + 1;
		res.status(200).json
		({ message: 'Welcome to PayPorter API You have visited '+ numOfTimeVisited +' Times, session_id is: '+ req.session.id});
	
	 } else {
		req.session.page_views = 1;
		res.status(200).json
		({ message: 'Welcome to PayPorter  '+ req.session.id});
	}

});

app.get('/getTime', (req, res) => {
	var currTime = new Date().getTime();
	var dateinwords = new Date().getDate()+" "+new Date().getMonth()+" "+new Date().getUTCFullYear();
	res.status(200).json({ dummy:"result",message: currTime,stringVal:dateinwords });
});


app.use('/api', router);
